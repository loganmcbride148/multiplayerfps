﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    [SerializeField]
    RectTransform ThrusterFuelFill;

    private PlayerController controller;

    private void Update()
    {
        setFillAmount(controller.getThrusterFuelAmount());
    }

    void setFillAmount(float amount)
    {
        ThrusterFuelFill.localScale = new Vector3(1f, amount, 1f);
    }

    public void SetController(PlayerController inController)
    {
        controller = inController;
    }
}
