﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{
    [SerializeField]
    private Camera cam;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraRotationX = 0f;
    private float currentCameraRotationX = 0f;
    private Vector3 thrusterForce = Vector3.zero;

    [SerializeField]
    private float cameraRotationLimit = 85f;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    //run ever physics iteration
    private void FixedUpdate()
    {
        ExecuteMovement();
        ExecuteRotation();
    }

    //Gets movement vector
    public void Move(Vector3 inVel)
    {
        velocity = inVel;
    }

    public void ApplyThrusterForce(Vector3 inForce)
    {
        thrusterForce = inForce;
    }

    //execute movement based on velocity variable
    private void ExecuteMovement()
    {
        if(velocity != Vector3.zero)
        {
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }
        if(thrusterForce != Vector3.zero)
        {
            rb.AddForce(thrusterForce * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
    }

    public void Rotate(Vector3 inRot)
    {
        rotation = inRot;
    }
    public void RotateCamera(float inRot)
    {
        cameraRotationX = inRot;
    }

    private void ExecuteRotation()
    {
        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));
        if(cam != null)
        {
            //Set rotation and clamp it between -85 and 85
            currentCameraRotationX -= cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);

            //Apply rotation to the transfrom of the camera.
            cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f);
        }
    }
}
