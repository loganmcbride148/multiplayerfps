﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerWeapon
{
    public string name = "Glock";
    public int damage = 20;
    public float range = 100f;

    public float fireRate = 0f;

    public GameObject graphics;
}
